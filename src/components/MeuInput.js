import React from 'react';

const MeuInput = ({ label, value, onChangeFunc }) => {
  return (
    <label>
      {label}
      <input
        type="number"
        onChange={val => {
          onChangeFunc(val.target.value);
        }}
        value={value}
      />
    </label>
  );
};

export default MeuInput;
