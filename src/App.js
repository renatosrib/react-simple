import React from 'react';
import { Route } from 'react-router-dom';
import createHashHistory from 'history/createHashHistory';
import { Router } from 'react-router';
import Template from './pages/Template';

import Soma from './pages/Soma';
import PaginaSemNada from './pages/PaginaSemNada';

import './assets/css/app.css';

const history = createHashHistory();

const App = () => {
  return (
    <Router history={history}>
      <Template>
        <Route component={Soma} exact path="/" />
        <Route component={PaginaSemNada} exact path="/nada" />
      </Template>
    </Router>
  );
};

export default App;
