import { observable, action, runInAction, toJS } from 'mobx';
import axios from 'axios';
import envs from 'envs';

class SomaStore {
  @observable
  operandos = {
    operando1: '',
    operando2: '',
  };

  @observable resultado = null;

  @action
  setOperando1(valor) {
    this.operandos.operando1 = valor;
  }

  @action
  setOperando2(valor) {
    this.operandos.operando2 = valor;
  }

  @action
  calcularSoma() {
    axios
      .post(`${envs.host}/soma`, toJS(this.operandos))
      .then(response => {
        runInAction('Recebe resultado', () => {
          this.resultado = response.data.resultado;
        });
      })
      .catch(() => {
        runInAction('Recebe resultado', () => {
          this.resultado = 'houve um erro a o realizar a operação';
        });
      });
  }
}

export const somaStore = new SomaStore();
