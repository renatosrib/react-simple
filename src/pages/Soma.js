import React from 'react';
import { observer } from 'mobx-react';
import { somaStore } from '../stores/SomaStore';
import MeuInput from '../components/MeuInput';

@observer
class Soma extends React.Component {
  constructor(props) {
    super(props);
    somaStore.setOperando1 = somaStore.setOperando1.bind(somaStore);
    somaStore.setOperando2 = somaStore.setOperando2.bind(somaStore);
  }

  render() {
    let mensagemComResultado = null;
    if (somaStore.resultado) {
      mensagemComResultado = (
        <div>
          O resultado da somatória de {somaStore.operandos.operando1} + {somaStore.operandos.operando2} é{' '}
          {somaStore.resultado}
        </div>
      );
    }
    return (
      <section>
        <form>
          <MeuInput label="Operando 1" onChangeFunc={somaStore.setOperando1} value={somaStore.operandos.operando1} />
          <MeuInput label="Operando 2" onChangeFunc={somaStore.setOperando2} value={somaStore.operandos.operando2} />
          <button
            onClick={e => {
              e.preventDefault();
              somaStore.calcularSoma();
            }}
          >
            Somar!
          </button>
        </form>
        {mensagemComResultado}
      </section>
    );
  }
}

export default Soma;
