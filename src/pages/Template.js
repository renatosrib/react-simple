import React from 'react';
import { Link } from 'react-router-dom';

class Template extends React.Component {
  render() {
    return (
      <div>
        <div className="menu">
          <ul>
            <li>
              <Link to="/">Home </Link>
            </li>
            <li>
              <Link to="/nada">Página 2</Link>
            </li>
          </ul>
        </div>
        <section>
          <main>
            <div>
              <div>{this.props.children}</div>
            </div>
          </main>
        </section>
      </div>
    );
  }
}

export default Template;
