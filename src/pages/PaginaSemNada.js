import React from 'react';

class PaginaSemNada extends React.Component {
  render() {
    return (
      <section>
        <p>Ao voltar para a página anterior ela não vai perder o estado.</p>
      </section>
    );
  }
}

export default PaginaSemNada;
