# Instruções

Para rodar o projeto é necessário instalar o Yarn (https://yarnpkg.com/pt-BR/). É possível rodar o projeto diretamente com o NPM, mas para isto é necessário adaptar o `package.json`.

Para subir o back end use o comando `yarn server`

Para subir o front end use o comando `yarn dev`. Ele ficará disponível no seguinte endereço: http://localhost:8000/
