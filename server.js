const express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.get('/', (req, res) => res.send('Hello World!'));

app.post('/api/soma', (req, res) => {
  let resultadoSoma = parseInt(req.body.operando1) + parseInt(req.body.operando2);
  let resposta = { resultado: resultadoSoma };
  res.send(JSON.stringify(resposta));
});

app.listen(3000, () => console.log('Example app listening on port 3000!'));
